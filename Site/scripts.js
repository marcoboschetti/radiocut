var maxElements = 200

$(document).ready(function(){
 // Loads JSONS from Server in GO
 $.get( "http://localhost:8080/cutsByUsername", function( cutsByUserRaw ) {
   $.get( "http://localhost:8080/playsByUsername", function( playsByUserRaw ) {
    var userMap = mergeListsUsername(cutsByUserRaw, playsByUserRaw);
    plotUserLists(cutsByUserRaw, playsByUserRaw, userMap, "#cutsByUser", "#playsByUser", "user");
  });
 });

 $.get( "http://localhost:8080/cutsByRadio", function( cutsByRadioRaw ) {
   $.get( "http://localhost:8080/playsByRadio", function( playsByRadioRaw ) {
    var userMap = mergeListsRadio(cutsByRadioRaw, playsByRadioRaw);
    plotUserLists(cutsByRadioRaw, playsByRadioRaw, userMap, "#cutsByRadio", "#playsByRadio", "radio");
  });
 });
 
});

 /*
var cutsByUserRaw = $.parseJSON(jsonCutsByUsername)
var playsByUserRaw = $.parseJSON(jsonPlaysByUsername)
//Loading JSONS from static files, just because GIST cant handle a go server =/
  var userMap = mergeLists(cutsByUserRaw, playsByUserRaw);
  plotUserLists(cutsByUserRaw, playsByUserRaw, userMap);

});
*/
function mergeListsUsername(cutsByUserRaw, playsByUserRaw){
 var cutsByUsername = cutsByUserRaw.data
 var playsByUsername = playsByUserRaw.data

 var userMap = {}
 for(var i = 0; i < cutsByUsername.length; i++){
  for(var j = 0; j < playsByUsername.length; j++){
    if(cutsByUsername[i].Username ==  playsByUsername[j].Username){
      userMap[cutsByUsername[i].Username] = i
    }  
  }
}
return userMap;
};


function mergeListsRadio(cutsByUserRaw, playsByUserRaw){
 var cutsByRadio = cutsByUserRaw.data
 var playsByRadio = playsByUserRaw.data

 var userMap = {}
 for(var i = 0; i < cutsByRadio.length; i++){
  for(var j = 0; j < playsByRadio.length; j++){
    if(cutsByRadio[i].RadioName ==  playsByRadio[j].RadioName){
      userMap[cutsByRadio[i].RadioName] = i
    }  
  }
}
return userMap;
};


function plotUserLists(cutsByUserRaw, playsByUserRaw, userMap, leftContainer, rightContainer, idPrefix){
  var cutsByUsername = cutsByUserRaw.data

  for(var i = 0; i < cutsByUsername.length; i++){
    var item = cutsByUsername[i];
    if(item.Username){
      var index =userMap[item.Username]
    }else{
      var index =userMap[item.RadioName]
    }
    if(index < maxElements){
      if(item.Username){
        $( leftContainer ).append( "<div class=\"radio-label username\" id=\""+idPrefix+"cut-"+index+"\" index=\""+index+"\" onclick=\"getSlugsFromUsername('"+item.Username+"');\">"+truncate(item.Username)+" </div><br>");
      }else{
        var index =userMap[item.RadioName]
        $( leftContainer ).append( "<div class=\"radio-label username\" id=\""+idPrefix+"cut-"+index+"\" index=\""+index+"\" onclick=\"getSlugsFromRadioname('"+item.RadioName+"');\">"+truncate(item.RadioName)+" </div><br>");
      }
    }
  }

  var minAngle = 360
  var maxAngle = 0
  var maxWordLength = 0

  var playsByUsername = playsByUserRaw.data
  for(var i = 0; i < playsByUsername.length; i++){
    var item = playsByUsername[i];

    if(item.Username){
      var index =userMap[item.Username]
    }else{
      var index =userMap[item.RadioName]
    }
    //Collect data for plot
    if(index < maxElements){
     // $("#playsByUser").append( "<div class=\"radio-label\" id=\"play-"+index+"\">"+item.Username+" ("+item.Count+")</div><br>");
     if(item.Username){
       $( rightContainer ).append( "<div class=\"radio-label\" id=\""+idPrefix+"play-"+index+"\" index=\""+index+"\" onclick=\"getSlugsFromUsername('"+item.Username+"');\">"+truncate(item.Username)+" </div><br>");
     }else{
      $( rightContainer ).append( "<div class=\"radio-label\" id=\""+idPrefix+"play-"+index+"\" index=\""+index+"\" onclick=\"getSlugsFromRadioname('"+item.RadioName+"');\">"+truncate(item.RadioName)+" </div><br>");
    }


    var width = $("#"+idPrefix+"cut-"+index).width();
    if(width > maxWordLength){
      maxWordLength = width;
    }
  }
}

indexAngles = {}
for(var i = 0; i < playsByUsername.length; i++){
 var item = playsByUsername[i];

 if(item.Username){
  var index =userMap[item.Username]
}else{
  var index =userMap[item.RadioName]
}

if(index < maxElements){
  var angle = getLineAngle($("#"+idPrefix+"cut-"+index), $("#"+idPrefix+"play-"+index), maxWordLength);
  if(angle > maxAngle){
    maxAngle = angle
  }
  if(angle < minAngle){
    minAngle = angle
  }
  indexAngles[index] = angle
}
}

for(var i = 0; i < playsByUsername.length; i++){
  var item = playsByUsername[i];

  if(item.Username){
    var index =userMap[item.Username]
  }else{
    var index =userMap[item.RadioName]
  }
  if(index < maxElements){
    var line = createLine($("#"+idPrefix+"cut-"+index), $("#"+idPrefix+"play-"+index),indexAngles[index], maxWordLength, minAngle, maxAngle, index, idPrefix);
    $("#main-container").append(line);

  //  line = createLinePrefix($("#"+idPrefix+"cut-"+index), indexAngles[index], maxWordLength, minAngle, maxAngle, index);
  //  $("#main-container").append(line);
}
}


for(var i = 0; i < playsByUsername.length; i++){
  var item = playsByUsername[i];

  if(item.Username){
    var index =userMap[item.Username]
  }else{
    var index =userMap[item.RadioName]
  }

  var paintAll =  function() {
    var index = $(this).attr("index");
    $("#usercut-"+index).addClass( "usernameHovered" );
    $("#userplay-"+index).addClass( "playHovered" );
    $("#userline-"+index).addClass( "lineHovered" );
  };

  var paintAllBis = function() {
    var index = $(this).attr("index");
    $("#radiocut-"+index).addClass( "usernameHovered" );
    $("#radioplay-"+index).addClass( "playHovered" );
    $("#radioline-"+index).addClass( "radioHovered" );
  };

  var unpaintAll = function(){
   var index = $(this).attr("index");
   $("#usercut-"+index).removeClass( "usernameHovered" );
   $("#userplay-"+index).removeClass( "playHovered" );
   $("#userline-"+index).removeClass( "lineHovered" );
 };

 var unpaintAllBis = function(){
  var index = $(this).attr("index");
  $("#radiocut-"+index).removeClass( "usernameHovered" );
  $("#radioplay-"+index).removeClass( "playHovered" );
  $("#radioline-"+index).removeClass( "radioHovered" );
};

$("#usercut-"+index).hover( paintAll, unpaintAll);
$("#userplay-"+index).hover( paintAll, unpaintAll);
$("#radiocut-"+index).hover( paintAllBis, unpaintAllBis);
$("#radioplay-"+index).hover( paintAllBis, unpaintAllBis);
}

}

function getLineAngle(el1, el2, maxWordLength){
  var y1 = el1.offset().top + el1.height() / 2;
  var x1 = el1.offset().left+ maxWordLength + 5;

  var y2 = el2.offset().top+el2.height() / 2;
  var x2 = el2.offset().left - 5;

  var angleDeg = Math.atan2(y2 - y1, x2 - x1) * 180 / Math.PI;

  return angleDeg
}


function createLinePrefix(el1, angleDeg, maxWordLength, minAngle, maxAngle, index){
  var y1 = el1.offset().top + el1.height() / 2;
  var x1 = el1.offset().left + el1.width();

  var y2 = el1.offset().top + el1.height() / 2;
  var x2 = el1.offset().left+ maxWordLength + 5;

  var colorValue = (angleDeg - minAngle) / (maxAngle - minAngle)
  
  var color = heatMapColorforValue(colorValue * 100);
  var line ='<svg class="connecting-line" height="600em" width="100%">'
  +'<line x1="'+x1+'" y1="'+y1+'" x2="'+x2+'" y2="'+y2+'" style="stroke:'+color+';stroke-width:2" id="line-prefix-'+index+'"/>'
  +'</svg>'
  return  line;
};

function createLine(el1, el2, angleDeg, maxWordLength, minAngle, maxAngle, index, idPrefix){
  var y1 = el1.offset().top+el1.height() / 2;
  var x1 = el1.offset().left + el1.width();

  var y2 = el2.offset().top+el2.height() / 2;
  var x2 = el2.offset().left - 5;

  var colorValue = (angleDeg - minAngle) / (maxAngle - minAngle)
  
  var color = heatMapColorforValue(colorValue * 100);


/* RECT LINE
  var line ='<svg class="connecting-line" height="600em" width="100%">'
  +'<line x1="'+x1+'" y1="'+y1+'" x2="'+x2+'" y2="'+y2+'" style="stroke:'+color+';stroke-width:2" />'
  +'</svg>'
  */
  var line = '<svg class="connecting-line" height="600em" width="100%">'+
  '<path d="M '+x1+' '+y1+' q '+(x2-x1)/8+' '+(-1)*(y2-y1)/8+' '+(x2-x1)+' '+(y2-y1)+
  '" stroke="'+color+'" stroke-width="2" fill="none" id="'+idPrefix+'line-'+index+'"/>'+
  '</svg>' 

  return  line;
};

function heatMapColorforValue(n){
  R = Math.round((255 * n) / 100)
  G = Math.round((255 * (100 - n)) / 100 )  
  B = 0
  return "rgb(" + R + ","+ G+", "+B+")";
}

function truncate(string){
 if (string.length > 19)
  return string.substring(0,19)+'...';
else
  return string;
};


// Handle click callbacks
function getSlugsFromUsername(username){
$("#cuts-container").empty();
$("#cuts-container").append("<h3>Loading Top 5 slugs ...</h3>")
$.get( 'http://localhost:8080/slugsByUsername?username='+username, function( slugs ) {
$("#cuts-container").empty();
  for(var i = 0; i < slugs.data.length; i++){
    var slugItem = slugs.data[i]

    var extra = (i == 0? '<div class="col-md-1"></div>':"");
    $('#cuts-container').append(extra+'<div class="col-md-2 cut-iframe-container" id="cut-iframe-'+i+'"> <div>Plays: <strong>'+slugItem.Count+'</strong></div></div>')

    var f = document.createElement('iframe');
    f.src = '//ar.radiocut.fm/audiocut/embed/mini/'+slugItem.Slug; 
    f.width = "100%"; 
    f.height = "65px";
    $('#cut-iframe-'+i).append(f);
  }
});
}

// Handle click callbacks
function getSlugsFromRadioname(radioname){
$("#cuts-container").empty();
$("#cuts-container").append("<h3>Loading Top 5 slugs ...</h3>")
$.get( 'http://localhost:8080/slugsByRadio?radioname='+radioname, function( slugs ) {
  $("#cuts-container").empty();
  for(var i = 0; i < slugs.data.length; i++){
    var slugItem = slugs.data[i]

    var extra = (i == 0? '<div class="col-md-1"></div>':"");
    $('#cuts-container').append(extra+'<div class="col-md-2 cut-iframe-container" id="cut-iframe-'+i+'"> <div>Plays: <strong>'+slugItem.Count+'</strong></div></div>')

    var f = document.createElement('iframe');
    f.src = '//ar.radiocut.fm/audiocut/embed/mini/'+slugItem.Slug; 
    f.width = "100%"; 
    f.height = "65px";
    $('#cut-iframe-'+i).append(f);
  }
});
}