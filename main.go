package main

import (
	"github.com/gin-gonic/gin"
	"github.com/go-pg/pg"
	"net/http"
)

func main() {
	db := pg.Connect(&pg.Options{
		User:     "radiocut",
		Password: "radiocut",
	})

	r := gin.Default()

	r.GET("/", func(c *gin.Context) {
		http.ServeFile(c.Writer, c.Request, "./Site/index.html")
	})

	r.Static("/Site", "./Site/")

	r.GET("/cutsByUsername", func(c *gin.Context) {
		res := getCutsByUsername(db)
		c.JSON(200, gin.H{
			"data": res,
		})
	})

	r.GET("/playsByUsername", func(c *gin.Context) {
		res := getPlaysByUsername(db)
		c.JSON(200, gin.H{
			"data": res,
		})
	})

	r.GET("/cutsByRadio", func(c *gin.Context) {
		res := getCutsByRadio(db)
		c.JSON(200, gin.H{
			"data": res,
		})
	})

	r.GET("/playsByRadio", func(c *gin.Context) {
		res := getPlaysByRadio(db)
		c.JSON(200, gin.H{
			"data": res,
		})
	})

	r.GET("/slugsByUsername", func(c *gin.Context){
		q := c.Request.URL.Query()
		username := q["username"][0]

		res := getTopSlugsByUsername(db, username)
		c.JSON(200, gin.H{
			"data": res,
		})
	})

	r.GET("/slugsByRadio", func(c *gin.Context){
		q := c.Request.URL.Query()
		radioname := q["radioname"][0]

		res := getTopSlugsByRadio(db, radioname)
		c.JSON(200, gin.H{
			"data": res,
		})
	})

	r.Run() // listen and serve on 0.0.0.0:8080
}
