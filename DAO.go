package main

import (
	"fmt"
	"github.com/go-pg/pg"
	"time"
)

type AudioCutFullEntry struct {
	TableName struct{} `sql:"audiocut"`

	Id          int
	Slug        string
	Radio_id    string
	Radio_name  string
	Show_id     string
	Show_name   string
	Start       time.Time
	Length      int
	Username    string
	Description string
	Title       string
	Created     time.Time
	Play        int
}

func (u AudioCutFullEntry) String() string {
	return fmt.Sprintf("AudioCut<%d %s %v>", u.Id, u.Username, u.Slug)
}

type StringCount []struct {
	Username string
	Count    int
}

type StringCountRadio []struct {
	RadioName string
	Count     int
}

func getPlayOverCountByUsername(db *pg.DB) StringCount {
	var res StringCount

	return res
}

func getCutsByUsername(db *pg.DB) StringCount {
	var res StringCount

	db.Model(&AudioCutFullEntry{}).
		Column("username").
		ColumnExpr("count(*) AS count").
		Group("username").
		Order("count DESC").
		Select(&res)

	return res
}

func getPlaysByUsername(db *pg.DB) StringCount {
	var res StringCount

	db.Model(&AudioCutFullEntry{}).
		Column("username").
		ColumnExpr("sum(play) AS count").
		Group("username").
		Order("count DESC").
		Select(&res)

	return res
}

func getCutsByRadio(db *pg.DB) StringCountRadio {
	var res StringCountRadio

	db.Model(&AudioCutFullEntry{}).
		Column("radio_name").
		ColumnExpr("count(*) AS count").
		Group("radio_name").
		Order("count DESC").
		Select(&res)

	return res
}

func getPlaysByRadio(db *pg.DB) StringCountRadio {
	var res StringCountRadio

	db.Model(&AudioCutFullEntry{}).
		Column("radio_name").
		ColumnExpr("sum(play) AS count").
		Group("radio_name").
		Order("count DESC").
		Select(&res)

	return res
}

type StringSlugs []struct {
	Count int
	Slug    string
}

// ** Agregar recortes por usuario on click
func getTopSlugsByUsername(db *pg.DB, username string)StringSlugs {
	var res StringSlugs

	db.Model(&AudioCutFullEntry{}).
		Where("username = ?", username).
		ColumnExpr("sum(play) AS count").
		Column("slug").
		Group("username", "slug").
		Order("count DESC").
		Limit(5).
		Select(&res)

	return res
}

func getTopSlugsByRadio(db *pg.DB, radioname string)StringSlugs {
	var res StringSlugs

	db.Model(&AudioCutFullEntry{}).
		Where("radio_name = ?", radioname).
		ColumnExpr("sum(play) AS count").
		Column("slug").
		Group("radio_name", "slug").
		Order("count DESC").
		Limit(5).
		Select(&res)

	return res
}